import 'dart:math' as math;

import 'dart:math';

void main(List<String> args) {
  // Comentario convencional com slashes duplos para declaracao e atribuicao de valor inteiro à variavel x.
  int x = 10;

  /*
    Comentario de multiplas 
    linhas para
    declaracao e atribuicao de ponto flutuante à variavel y.
  */
  double y = 8.98765;

  /// Comentario de documentacao...

  /**
   * Outra forma de comentario de documentacao, neste caso utilizamos apenas para demonstrar o
   * comando de saida padrao em console do Dart
   */
  print(x);
  print(y);

  /*
    Em Dart os valores numericos sao objetos, assim sendo podemos tirar proveito das funcionalidades.
    Abaixo veremos alguns exemplos:
  */
  print("$x é par? Resposta: ${x.isEven}");
  print("5 é ímpar? Resposta: ${5.isOdd}");
  print("10 é um double agora? Resposta: ${x.toDouble() is double}");

  // Declaracao e Manipulacao de tipos de Dados em Variaveis
  /*
    O Dart é uma Linguagem fortemente tipada, ou seja as variaveis devem ter expliciamente 
    especificados os tipos de dados a serem armazenados, com ressalvas... Podemos faze-lo de três maneiras:

    1. var x;             -- Definicao dinamica de tipo por inferencia.
    2. <TipoDeDado> x;    -- Definicao estatica.
    3. dynamic x;         -- Definicao por inferencia e possibilidade de troca de tipo de dado. em tempo de execucao.

    Ja utilizamos a definicao e atribuicao estatica nos exemplos acima, entao os exemplos abaixo
    utilizaremos a tipagem dinamica.
  */

  //Tipagem dinamica
  var a = 234;
  var b = 234.567;
  print("A variavel a é do tipo ${a.runtimeType}");
  print("A variavel b é do tipo ${b.runtimeType}");

  dynamic c = a;
  print("A variavel c é do tipo ${c.runtimeType}");
  c = b;
  print("A variavel c agora é do tipo ${c.runtimeType}");

  //Booleanos
  /*
    Em Dart possuimos apenas os objetos true e false
    Expressoes do tipo if(varialvel_nao_booleana) nao sao aceitas;
    Sao aceitas apenas a utilizacao de variaveis nao booleanas que executem metodos com retornos booleanos
      - if(variavel_nao_booleana.executaMetodoComRetornoBooleano).
    As multiplas possibilidades de valores aceitos como booleanos das linguagens web nao sao aceitos.
  */

  //Listas
  /*
    Lista em dart sao analogoas aos arrays das linguagens de programacao convencionais.
    Abaixo seguem alguns exemplos.
  */

  List lista1 = [10, 23, 4, 45, 1, 61, 74, 8, 98, 7];
  var lista2 = [19, 2, 36, 14, 5, 76];
  Object lista3 = [1, 2, 3, 4, 5, 6];

  /*
    Listas sao colecoes indexadas, ou seja o indice faz garante a posicao de identificacao o objeto na lista,
    para acessa-lo basta acessar o objeto referenciando seu indice na lista, como no exemplo abaixo.
  */

  print("Quarto item da lista1: ${lista1[3]}");
  print("Tamanho da lista2: ${lista2.length}");

  //Percorrer a lista1
  print("\nLista 1:");
  lista1.forEach((element) {
    print("Elemento: $element");
  });

  //Ordenar Lista 1
  lista1.sort((a, b) => a.compareTo(b));
  print(lista1);

  /*
    Maps sao listas que utilizam chaves ao inves de indices para acessar seus valores, para acessar seus elementos, 
    basta acessar o objeto referenciando uma chave, como no exemplo abaixo.
  */

  Map atoresStarWars = {
    "Luke Skywalker": "Mark Hammil",
    "Leia Organa": "Carrie Fischer",
    "Han Solo": "Harrison Ford"
  };

  // Percorrer Map
  print("\nAtores Star Wars");
  atoresStarWars.forEach((key, value) {
    print("personagem: $key - ator: $value");
  });

  var atoresVingadores = Map();
  atoresVingadores["Homen de Ferro"] = "Robert Downey Jr";
  atoresVingadores["Capitão América"] = "Chris Evans";
  atoresVingadores["Viúva Negra"] = "Scarlett Johansson";

  // Percorrer Map
  print("\nAtores Vingadores");
  atoresVingadores.forEach((key, value) {
    print("personagem: $key - ator: $value");
  });

  // Acessar elemento
  print("\nAtriz de Viúva Negra: ${atoresVingadores['Viúva Negra']}");

  // Listar chaves
  print("\nPersonagens de Vigadores: ${atoresVingadores.keys}");

  // Listar valores
  print("\nAtores de Vigadores: ${atoresVingadores.values}");

  // Loops: For, Foreach, For In, While, Do While

  print("\nLoop: FOR");
  for (var i = 0; i < lista2.length; i++) {
    print("lista1[$i] = ${lista2[i]}");
  }

  print("\nLoop: FOREACH Funcional");
  lista1.forEach((element) {
    print("lista1: $element");
  });

  print("\nLoop: FOR IN");
  for (var item in lista3) {
    print("item: $item");
  }

  // Os Loops While e Do While são iguais as demais linguagens...

  var retangulo = Retangulo(10, 89);
  var quadrado = Quadrado(23);
  var circulo = Circulo(45);

  print("Area Retangulo: ${retangulo.area()}");
  print("Area Quadrado: ${quadrado.area()}");
  print("Area Circulo: ${circulo.area().toStringAsPrecision(5)}");
  var professor = Professor("Esdras");
  print("${professor.nome} é do tipo ${professor.runtimeType}");
  professor.programarEmFlutter();
  var superHeroi = SuperHeroi("Curupira");
  superHeroi.exibeVingador();
}

// Orientacao a Objetos
//  Declaracao de classe e atributos
class Casa {
  // Atributos publicos
  int comodos;
  double area;
  bool garagem;
  int banheiros;

  Casa({this.comodos, this.area, this.banheiros, this.garagem});
}

abstract class Forma {
  double area();
}

class Retangulo extends Forma {
  double comprimento, largura;

  Retangulo(this.comprimento, this.largura);

  @override
  double area() {
    return comprimento * largura;
  }
}

class Circulo extends Forma {
  double raio;

  Circulo(this.raio);

  @override
  double area() {
    return math.pi * math.pow(raio, 2);
  }
}

class Quadrado extends Retangulo {
  double lado;
  Quadrado(this.lado) : super(lado, lado);
}

class Pessoa {
  String nome;
  Pessoa(this.nome);
}

class Professor extends Pessoa with Programador{
  Professor(String nome) : super(nome){
    pessoa = this;
  }
}

mixin Programador {
  Pessoa pessoa;

  programarEmFlutter(){
    print("${pessoa.nome} é DEV e programa em Dart!");
  }
}

mixin Vingador {

  Pessoa pessoa;

  bool possuiEscudo = false, possuiArmadura = false, 
  consegueEncolher = false, possuiMijounir = true;

  exibeVingador(){
    if(possuiEscudo){
      print("${pessoa.nome} com poderes do Capitão América.");
    } else if(possuiArmadura){
      print("${pessoa.nome} com inteligencia, armadura e humildade do Home de Ferro.");
    } else if (consegueEncolher){
      print("${pessoa.nome} com habilidades do Homem Formiga.");
    } else {
      print("${pessoa.nome} com poderes e piadas criativas do Thor.");
    }
  }
}

class SuperHeroi extends Pessoa with Vingador{
  SuperHeroi(String nome) : super(nome){
    pessoa = this;
  }
}
